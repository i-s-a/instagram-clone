const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const requireLogin = require('../middleware/requireLogin');
const Post = mongoose.model("Post");

router.get('/allposts', requireLogin, async (req, res) => {
    try {
        const posts = await Post.find()
        .populate("postedBy", "_id name photo")    //Include the id, name and photo of the user that posted this post
        .populate("comments.postedBy", "_id name")
        .sort("-createdAt") //createdAt automatically added to the model because in Post we used timestamps:true
        .exec();

        res.json(posts);
    }
    catch (error) {
        console.log(error.message);
        res.status(500).json({error:"Error retrieving posts"});
    }
});

router.get('/followingposts', requireLogin, async (req, res) => {
    try {
        const posts = await Post.find({postedBy:{$in:req.user.following}})
        .populate("postedBy", "_id name photo")    //Include the id, name and photo of the user that posted this post
        .populate("comments.postedBy", "_id name")
        .sort("-createdAt") //createdAt automatically added to the model because in Post we used timestamps:true
        .exec();

        res.json(posts);
    }
    catch (error) {
        console.log(error.message);
        res.status(500).json({error:"Error retrieving posts"});
    }
});

router.post('/createpost', requireLogin, async (req, res) => {
    try {
        const {title, body, imageUrl} = req.body;
    
        if (!title || !body || !imageUrl) {
            return res.status(422).json({error:"Please include all the fields"});
        }
    
        //Don't want to store the password of the user with the post
        req.user.password = undefined;
    
        const post = new Post({
            title,
            body,
            photo:imageUrl,
            postedBy:req.user
        });
    
        const savedPost = await post.save();
        res.json({post:savedPost});
    }
    catch (error) {
        console.log(error.message);
        res.status(500).json({error:"Error creating post"});
    }
});

router.post('/editpost/:postId', requireLogin, async (req, res) => {
    try {
        const post = await Post.findById(req.params.postId)
        .populate("postedBy", "_id")
        .exec();

        if (post.postedBy._id.toString() === req.user._id.toString()) {
            post.title = req.body.title;
            post.body = req.body.body;
            const updatedPost = await post.save();
            res.json({message:"Post updated successfully"});
        }
        else {
            res.status(403).json("Not allowed to edit this post");
        }
    }
    catch (error) {
        console.log(error.message);
        res.status(404).json("Failed to find post");
    }
})

router.get('/mypost/:postId', requireLogin, async (req, res) => {
    try {
        const post = await Post.findById(req.params.postId)
        .populate("postedBy", "_id")
        .exec();

        if (post.postedBy._id.toString() === req.user._id.toString()) {
            res.json(post);
        }
        else {
            res.status(403).json("Not allowed access to this post");
        }        
    } catch (error) {
        console.log(error.message);
        res.status(404).json("Failed to find post");        
    }
})

router.get('/myposts', requireLogin, async (req, res) => {
    try {
        const posts = await Post.find({postedBy:req.user._id})
        .populate("postedBy", "_id name")
        .exec();
    
        res.json(posts);
    }
    catch (error) {
        console.log(error.message);
        res.status(500).json({error:"Error retrieving posts"});
    }    
})

router.put('/like', requireLogin, async (req, res) => {
    try {
        const updatedPost = await Post.findByIdAndUpdate(req.body.postId, {
            $push:{likes:req.user._id}
        }, {
            new: true
        })
        .populate("postedBy", "_id name photo")
        .populate("comments.postedBy", "_id name")    
        .exec();

        res.json(updatedPost);
    }
    catch (error) {
        console.log(error.message);
        res.status(422).json("Error updating post with like");
    }
})

router.put('/unlike', requireLogin, async (req, res) => {
    try {
        const updatedPost = await Post.findByIdAndUpdate(req.body.postId, {
            $pull:{likes:req.user._id}
        }, {
            new: true
        })
        .populate("postedBy", "_id name photo")
        .populate("comments.postedBy", "_id name")
        .exec();

        res.json(updatedPost);
    }
    catch (error) {
        console.log(error.message);
        res.status(422).json("Error updating post with unlike");
    }
})

router.put('/comment', requireLogin, async (req, res) => {
    try {
        const comment = {
            text:req.body.text,
            postedBy:req.user._id
        }
    
        const updatedPost = await Post.findByIdAndUpdate(req.body.postId, {
            $push:{comments:comment}
        }, {
            new: true
        })
        .populate("comments.postedBy", "_id name")
        .populate("postedBy", "_id name photo")
        .exec();

        res.json(updatedPost);
    }
    catch {
        console.log(error.message);
        res.status(422).json("Error updating post with comment");
    }
})

router.delete('/deletepost/:postId', requireLogin, async (req, res) => {
    try {
        const post = await Post.findById(req.params.postId)
        .populate("postedBy", "_id")
        .exec();

        if (post.postedBy._id.toString() === req.user._id.toString()) {
            const deletedPost = await post.remove();
            //TODO: returning the full result for now. This contains everything related
            //to the deleted post (e.g. title, body, array of comments, etc.). Do we
            //really need to return all of this? Can we just return result._id, which
            //is the id of the deleted post?
            res.json(deletedPost);
        }
        else {
            return res.status(403).json("Not allowed to delete this post");
        }
    }
    catch (error) {
        console.log(error.message);
        res.status(404).json("Failed to find post");
    }
})

router.delete('/deletecomment/:postId/:commentId', requireLogin, async (req, res) => {
    
    try {
        //We only allow a comment to be deleted by the user (req.user._id) that created the comment
        const updatedPost = await Post.findByIdAndUpdate(req.params.postId, {
            $pull:
            {
                'comments': {
                    '_id': req.params.commentId,
                    'postedBy': req.user._id
                }
            }
        }, {new:true})
        .populate("comments.postedBy", "_id name")
        .populate("postedBy", "_id name photo")
        .exec();

        res.json(updatedPost);
    }
    catch (error) {
        console.log(error.message);
        res.status(422).json("Error deleting comment from post");
    }
})

module.exports = router;