const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const User = mongoose.model("User");
const crypto = require('crypto');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const {JWT_SECRET, SENDGRID_KEY, FROM_EMAIL, EMAIL_URL} = require('../config/keys');
const {requireLogin} = require('../middleware/requireLogin');
const nodemailer = require('nodemailer');
const sendgridTransport = require('nodemailer-sendgrid-transport');

const transporter = nodemailer.createTransport(sendgridTransport({
    auth:{
        api_key:SENDGRID_KEY
    }
}));

router.post('/signup', async (req, res) => {

    const {name, email, password, photo} = req.body;
    //NOTE: photo is optional. If it is not provided we will assign
    //a default photo
    if (!name || !email || !password) {
        return res.status(422).json({error:"Not all fields provided"});
    }

    try {
        let savedUser = await User.findOne({email:email}).exec();

        if (savedUser) {
            return res.status(422).json({error:"User already exists with that email"});
        }

        const hashedPassword = await bcrypt.hash(password, 12);
        const user = new User({
            name:name,
            email:email,
            password:hashedPassword,
            photo:photo
        });

        savedUser = await user.save();

        //TODO: Don't await the sendMail as we don't want to wait
        //until the email is sent to send the reply to the client.
        //That makes sense doesn't it?
        //TODO: Look at the article
        //https://blog.bitsrc.io/email-confirmation-with-react-257e5d9de725
        //This sends emails without using SendGrid. Look and see if
        //we can also use the same method here instead of using SendGrid
        transporter.sendMail({
            to:savedUser.email,
            from:FROM_EMAIL,
            subject:"Sign up successful",
            html:"<h1>Welcome to Instagram-clone</h1>"
        });

        res.json({message:"User saved successfully"});
    }
    catch (error) {
        console.log(error.message);
        res.status(500).json({error:"Signup internal error"});
    }
});

router.post('/signin', async (req, res) => {
    try {
        const {email, password} = req.body;

        if (!email || !password) {
            return res.status(422).json({error:"Please provide an email and password"});
        }
    
        const user = await User.findOne({email:email}).exec();
    
        if (!user) {
            return res.status(422).json({error:"Invalid email or passowrd"});
        }
    
        const passwordMatched = await bcrypt.compare(password, user.password);
        if (passwordMatched) {
            // res.json({message:"Successfully signed in"});
            const token = jwt.sign({_id:user._id}, JWT_SECRET);
            const {_id, name, email, followers, following, photo} = user;
            res.json({token, user:{_id,name,email,followers,following,photo}});
        }
        else {
            res.status(422).json({error:"Invalid email or passowrd"});
        }
    }
    catch (error) {
        console.log(error.message);
        res.status(500).json({error:"Error signing in"});
    }
});

router.post('/resetpassword', async (req, res) => {
    crypto.randomBytes(32, async (err, buffer) => {
        try {
            if (err) {
                console.log(err);
                res.status(500).json({error:"Error reseting password"});
            }
            else {
                const token = buffer.toString("hex");
    
                const user = await User.findOne({email:req.body.email}).exec();
        
                if (!user) {
                    return res.status(422).json({error:"Invalid email"});
                }
                
                user.resetToken = token;
                user.resetTokenExpiryDate = Date.now() + 3600000;   //Expires 1 hour from now
                const savedUser = await user.save();

                transporter.sendMail({
                    to:savedUser.email,
                    from:FROM_EMAIL,
                    subject:"Reset password",
                    html:`<p>You requested to reset your password</p>
                    <h5>Click on this <a href="${EMAIL_URL}/resetpassword/${token}">link</a> to reset your password</h5>`
                });
    
                res.json({message:"Check your email"});
            }    
        }
        catch (error) {
            console.log(error.message);
            res.status(500).json({error:"Error reseting password"});
        }        
    });
});

router.post('/newpassword', async (req, res) => {
    const newPassword = req.body.password;
    const userToken = req.body.token;

    try {
        const user = await User.findOne({resetToken:userToken, resetTokenExpiryDate:{$gt:Date.now()}}).exec();
        if (!user) {
            return res.status(422).json({error:"Invalid token or session has expired"});
        }

        const hashedPassword = await bcrypt.hash(newPassword, 12);
        user.password = hashedPassword;
        user.resetToken = undefined;
        user.resetTokenExpiryDate = undefined;
        const updatedUser = await user.save();
        res.json({message:"Password updated successfully"});
    }
    catch(error) {
        console.log(error.message);
        res.status(500).json({error:"Error updating password"});
    }
});

module.exports = router;