const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const requireLogin = require('../middleware/requireLogin');
// const createError = require('http-errors');
const Post = mongoose.model("Post");
const User = mongoose.model("User");

router.get('/users/:id', requireLogin, async (req, res) => {
    try {
        const user = await User.findById(req.params.id)
            .select("-password")    //We want all the fields except the password
            .exec();

        const posts = await Post.find({postedBy:req.params.id})
            .populate("postedBy", "_id name")
            .exec();

        res.json({user, posts});
    }
    catch (error) {
        //console.log(error.message);
        res.status(404).json({error:"User not found:" + error.stringValue});
    }
})


//The logged in user (req.user._id) wants to follow the user in
//the body of the request (req.body.followId). After this request
//the logged in user will have 'following' increased by one, and
//the user in the body of the request will have 'followers' increased
//by one.
router.put('/follow', requireLogin, async (req, res) => {

    try {
        const updatedFollowerUser = await User.findByIdAndUpdate(req.body.followId, {
            $push:{followers:req.user._id}
        }, {
            new:true
        }).exec();
    
        const updatedLoggedInUser = await User.findByIdAndUpdate(req.user._id, {
            $push:{following:req.body.followId}
        }, {
            new:true
        })
        .select("-password")
        .exec();
    
        res.json(updatedLoggedInUser);
    }
    catch (error) {
        // console.log(error);
        res.status(422).json({error:"Failed to update user: " + error.stringValue});
    }
})

router.put('/unfollow', requireLogin, async (req, res) => {
    try {
        const updatedUser = await User.findByIdAndUpdate(req.body.unfollowId, {
            $pull:{followers:req.user._id}
        }, {
            new:true
        })
        .exec();
        
        const updatedLoggedInUser = await User.findByIdAndUpdate(req.user._id, {
                $pull:{following:req.body.unfollowId}
        }, {
            new:true
        })
        .select("-password")
        .exec();
    
        res.json(updatedLoggedInUser);
    }
    catch (error) {
        //console.log(error);
        res.status(422).json({error:"Failed to update user: " + error.stringValue});
    }
})

router.put('/updateprofilepic', requireLogin, async (req, res) => {
    try {
        const updatedUser = await User.findByIdAndUpdate(req.user._id, {
            $set:{photo:req.body.photo}
        }, {
            new:true
        })
        .exec();

        res.json(updatedUser);
    }
    catch(error) {
        res.status(422).json({error:"Failed to update profile picture" + error.stringValue});
    }        
})

router.post('/search-users', requireLogin, async (req, res) => {
    try {
        let userPattern = new RegExp("^" + req.body.query);
        const users = await User.find({email:{$regex:userPattern}})
        .select("_id email")    //For now only want the ID and email. TODO: Perhaps include name as well?
        .exec();
        res.json({users});
    }
    catch (error) {
        console.log(error);
        res.status(422).json({error:"Failed search" + error.stringValue});
    }

})

module.exports = router;