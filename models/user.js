const mongoose = require('mongoose');
const {ObjectId} = mongoose.Schema.Types;

const userSchema = new mongoose.Schema({
    name: {
        type:String,
        required:true,
    },
    email: {
        type:String,
        required:true,
    },
    password: {
        type:String,
        required:true,
    },
    resetToken: {
        type:String
    },
    resetTokenExpiryDate: {
        type:Date
    },
    photo: {
        type:String,
        default:"https://res.cloudinary.com/mernimages/image/upload/v1589562935/no-profile-picture_mkrsz4.jpg"
    },
    followers: [
        {
            type:ObjectId,
            ref:"User"
        }
    ],
    following: [
        {
            type:ObjectId,
            ref:"User"
        }
    ]
});

userSchema.indexes({unique:'email'});

mongoose.model("User", userSchema);