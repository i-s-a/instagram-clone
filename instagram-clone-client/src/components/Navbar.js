import React, {useContext, useRef, useEffect, useState} from 'react'
import {Link, useHistory} from 'react-router-dom'
import {UserContext} from '../App'
import M from 'materialize-css'

const Navbar = () => {
    const searchModal = useRef(null);
    const [searchUsers, setSearchUsers] = useState('');
    const [userDetails, setUserDetails] = useState([]);
    const {state, dispatch} = useContext(UserContext);
    const history = useHistory();

    useEffect(() => {
      M.Modal.init(searchModal.current);
    }, []);

    const renderList = () => {
      if (state) {
        return [
          <li key="search"><i data-target="modal1" className="material-icons modal-trigger" style={{color:"black"}}>search</i></li>,
          <li key="profile"><Link to="/profile">Profile</Link></li>,
          <li key="createpost"><Link to="/create">Create Post</Link></li>,
          <li key="followingposts"><Link to="/followingposts">My Following Posts</Link></li>,
          <li key="logout">
            <button className="btn #c62828 red darken-3"
            onClick={() => {
              localStorage.clear();
              dispatch({type:"CLEAR"});
              history.push('/signin');
            }} >
                Logout
            </button>
          </li>
        ]
      }
      else {
        return [
          <li key="signin"><Link to="/signin">Sign In</Link></li>,
          <li key="signup"><Link to="/signup">Sign Up</Link></li>          
        ]
      }
    }

    const fetchUsers = async (query) => {
      setSearchUsers(query);

      try {
        const response = await fetch("/search-users", {
          method:"post",
          headers:{
              "Content-Type":"application/json",
              "Authorization":"Bearer " + localStorage.getItem("jwt")
          },
          body:JSON.stringify({
            query,
          })
        });

        const json = await response.json();
        //console.log(json);
        setUserDetails(json.users);
      }
      catch (error) {
        console.log(error);
      }
    }

    return (
        <nav>
        <div className="nav-wrapper white">
          <Link to={state ? "/" : "/signin"} className="brand-logo left">Instagram-clone</Link>
          <ul id="nav-mobile" className="right">
            {renderList()}
          </ul>
        </div>
        <div id="modal1" className="modal" ref={searchModal} style={{color:"black"}}>
          <div className="modal-content">
            <input
                type="text"
                placeholder="Search users"
                value={searchUsers}
                onChange={(e) => fetchUsers(e.target.value)}
            />

            <ul className="collection">
              {userDetails.map(user => {
                return <Link key={user._id} to={user._id !== state._id ? "/profile/" + user._id : "/profile"}
                onClick={() => {
                   //When the user clicks on a profile close the search modal box
                   //and clear the search query
                  M.Modal.getInstance(searchModal.current).close();
                  setSearchUsers('');
                }} ><li className="collection-item">{user.email}</li></Link>
              })}
            </ul>
          </div>
          <div className="modal-footer">
            <button className="modal-close waves-effect waves-green btn-flat" onClick={() => setSearchUsers('')}>Close</button>
          </div>
        </div>        
      </nav>
    );
}

export default Navbar;