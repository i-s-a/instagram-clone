import React, {useState, useContext} from 'react'
import {Link, useHistory} from 'react-router-dom'
import {UserContext} from '../../App'
import M from 'materialize-css'

const Signin = () => {
    const {state, dispatch} = useContext(UserContext);
    const history = useHistory();
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const PostLoginData = async () => {
        //See https://emailregex.com/
        if (!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email)) {
            M.toast({html: "Invalid email", classes:"#c62828 red darken-3"});
            return;
        }

        try {
            const response = await fetch("/signin", {
                method:"post",
                headers:{
                    "Content-Type":"application/json"
                },
                body:JSON.stringify({
                    email,
                    password
                })
            });

            const json = await response.json();

            if (json.error) {
                M.toast({html: json.error, classes:"#c62828 red darken-3"});
            }
            else {
                localStorage.setItem("jwt", json.token);
                localStorage.setItem("user", JSON.stringify(json.user));
                dispatch({type:"USER", payload:json.user});
                M.toast({html: "Sign in success", classes:"#43a047 green darken-1"});
                history.push("/");
            }
        }
        catch(error) {
            console.log(error);
        }
    }

    return (
        <div className="mycard">
            <div className="card auth-card input-field">
                <h2>Instagram</h2>
                <input
                    type="text"
                    placeholder="email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                />
                <input
                    type="password"
                    placeholder="password" 
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                />
                <button className="btn waves-effect waves-light #64b5f6 blue darken-1"
                onClick={() => PostLoginData()}>
                    Login
                </button>
                <h5>
                    <Link to="/signup">Don't have an account?</Link>
                </h5>
                <h6>
                    <Link to="/resetpassword">Forgot password?</Link>
                </h6>
            </div>
        </div>
    )
}

export default Signin;