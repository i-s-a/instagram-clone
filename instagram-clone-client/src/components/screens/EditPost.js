import React, {useEffect, useReducer} from 'react'
import M from 'materialize-css'
import {useHistory, useParams} from 'react-router-dom'

//TODO: When redirecting to the home page, use the following link that shows an
//example of how to redirect and automatically scroll to the post that was just
//edited. There are two different suggestions: 1. Adding anchors to the posts'
//div in Posts.js and installing "npm i react-router-hash-link". 2. Adding anchors
//to the posts' div and using scrollIntoView(...)
//https://stackoverflow.com/questions/48223566/using-anchor-tags-in-react-router-4

//NOTE: For now we intentionally don't allow the user to edit the picture in a post.
//We only allow them to edit the title and the body. If the user doesn't like the
//the picture then they will have to delete the post and updload a new post with the
//picture they want
const EditPost = () => {

    const initialState = {
        title:'',
        body:'',
        photo:''
    };

    const postReducer = (state, action) => {
        switch (action.type) {
            case 'TITLE':
                return {
                    ...state,
                    title:action.payload.title
                };
            case 'BODY':
                return {
                    ...state,
                    body:action.payload.body
                };
            case 'ALL':
                return {
                    title:action.payload.title,
                    body:action.payload.body,
                    photo:action.payload.photo
                };
            default:
                throw new Error();
        }
    }
    
    const history = useHistory();
    // const [title, setTitle] = useState("");
    // const [body, setBody] = useState("");
    const [postState, dispatch] = useReducer(postReducer, initialState);
    const {postId} = useParams();

    useEffect(() => {
        async function fetchPost() {
            try {
                const response = await fetch(`/mypost/${postId}`, {
                    headers: {
                        "Authorization":"Bearer " + localStorage.getItem("jwt")
                    }
                });
                
                const json = await response.json();
                dispatch({type:'ALL', payload:json});
            }
            catch (error) {
                console.log(error);
            }
        }

        fetchPost();
    }, [postId]);

    const uploadPostDetails = async () => {
        try {
            const response = await fetch(`/editpost/${postId}`, {
                method:"post",
                headers:{
                    "Content-Type":"application/json",
                    "Authorization":"Bearer " + localStorage.getItem("jwt")
                },
                body:JSON.stringify({
                    title:postState.title,
                    body:postState.body,
                })
            });

            const json = await response.json();

            if (json.error) {
                M.toast({html: json.error, classes:"#c62828 red darken-3"});
            }
            else {
                M.toast({html: "Post updated successfully", classes:"#43a047 green darken-1"});
                history.push("/")
            }

        }
        catch (error) {
            console.log(error);
        }
    }

    return (
        <div className="card input-field"
        style={{margin:"30px auto", maxWidth:"500px", padding:"20px", textAlign:"center"}}>
            <div className="card-image">
                <img src={postState.photo} alt={postState.title || ''} />
            </div>            
            <input
                type="text"
                placeholder="Title"
                defaultValue={postState.title || ''}
                onChange={(e) => dispatch({type:"TITLE", payload:{title:e.target.value}})} />
            <input
                type="text"
                placeholder="Body"
                defaultValue={postState.body || ''}
                onChange={(e) => dispatch({type:"BODY", payload:{body:e.target.value}})} />
            <button className="btn waves-effect waves-light #64b5f6 blue darken-1"
            onClick={() => uploadPostDetails()} >
                Edit Post
            </button>
        </div>
    )
}

export default EditPost;