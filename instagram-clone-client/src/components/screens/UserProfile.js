import React, {useEffect, useState, useContext} from 'react'
import {UserContext} from '../../App'
import {useParams} from 'react-router-dom'

const UserProfile = () => {

    const [userProfile, setUserProfile] = useState(null);
    const {state, dispatch} = useContext(UserContext);
    const {userId} = useParams();
    const [showFollow, setShowFollow] = useState(state ? !state.following.includes(userId) : true);

    useEffect(() => {
        async function fetchProfile() {
            try {
                const response = await fetch(`/users/${userId}`, {
                    headers: {
                        "Authorization":"Bearer " + localStorage.getItem("jwt")
                    }
                });

                const json = await response.json();
                setUserProfile(json);
            }
            catch (error) {
                console.log(error);
            }
        }

        fetchProfile();

    }, [userId]);

    const followUser = async () => {
        try {
            const response = await fetch('/follow', {
                "method":"put",
                headers: {
                    "Content-Type":"application/json",
                    "Authorization":"Bearer " + localStorage.getItem("jwt")
                },
                body: JSON.stringify({
                    followId:userId
                })
            });

            //The json response of the logged-in user with their list of 'followers' and 'following' updated
            const updatedUser = await response.json();

            //Update the global state and local storage with the changes to the logged in user, which will
            //have updated 'followers' and 'following' lists
            dispatch({type:"UPDATE", payload:{following:updatedUser.following, followers:updatedUser.followers}});
            localStorage.setItem("user", JSON.stringify(updatedUser));

            //We need to update the profile the logged-in user is veiwing to include the logged-in user as
            //a 'follower'
            //TODO: A bit ugly atm. Think if there is a better way to do this??
            setUserProfile((prevState) => {
                return {
                    ...prevState,
                    user: {
                        ...prevState.user,
                        followers:[...prevState.user.followers, updatedUser._id]
                    }
                }
            });

            setShowFollow(false);
        }
        catch (error) {
            console.log(error);
        }
    }

    const unfollowUser = async () => {
        try {
            const response = await fetch('/unfollow', {
                "method":"put",
                headers: {
                    "Content-Type":"application/json",
                    "Authorization":"Bearer " + localStorage.getItem("jwt")
                },
                body: JSON.stringify({
                    unfollowId:userId
                })
            });
            
            //The json response of the logged-in user with their list of 'followers' and 'following' updated
            const updatedUser = await response.json();
            
            //Update the global state and local storage with the changes to the logged in user, which will
            //have updated 'followers' and 'following' lists
            dispatch({type:"UPDATE", payload:{following:updatedUser.following, followers:updatedUser.followers}});
            localStorage.setItem("user", JSON.stringify(updatedUser));

            //We need to update the profile the logged-in user is veiwing to remove the logged-in user as
            //a 'follower'
            //TODO: A bit ugly atm. Think if there is a better way to do this??
            setUserProfile((prevState) => {
                const newFollowerList = prevState.user.followers.filter(item => item !== updatedUser._id);
                return {
                    ...prevState,
                    user: {
                        ...prevState.user,
                        followers:newFollowerList
                    }
                }
            });

            setShowFollow(true);
        }
        catch (error) {
            console.log(error);
        }
    }

    return (
        <>
        {userProfile 
        ?
            <div style={{maxWidth:"650px", margin:"0px auto"}}>
                <div style={{
                        display:"flex",
                        justifyContent:"space-around",
                        margin:"18px 0px",
                        borderBottom:"1px solid grey"
                    }}>
                    <div>
                        <img style={{width:"160px", height:"160px", borderRadius:"80px", "margin-top":"20px"}}
                        src={userProfile.user.photo}
                        alt="" />
                    </div>
                    <div>
                        <h4>{userProfile.user.name}</h4>
                        <h5>{userProfile.user.email}</h5>
                        <div style={{display:"flex", justifyContent:"space-between", width:"108%"}}>
                            <h6>{userProfile.posts.length} posts</h6>
                            <h6>{userProfile.user.followers.length} followers</h6>
                            <h6>{userProfile.user.following.length} following</h6>
                        </div>
                        {showFollow
                        ?
                            <button style={{margin:"10px"}} className="btn waves-effect waves-light #64b5f6 blue darken-1"
                            onClick={() => followUser()} >
                                Follow
                            </button>
                        :
                            <button style={{margin:"10px"}} className="btn waves-effect waves-light #64b5f6 blue darken-1"
                            onClick={() => unfollowUser()} >
                                Unfollow
                            </button>
                    }
                    </div>
                </div>
                <div className="gallery">
                    {
                        userProfile.posts.map(item => {
                            return (
                                <img className="item" key={item._id} src={item.photo} alt={item.title} />
                            )
                        })
                    }
                </div>
            </div>        
        : 
            <h2>Loading...</h2>}
        </>
    )
}

export default UserProfile;