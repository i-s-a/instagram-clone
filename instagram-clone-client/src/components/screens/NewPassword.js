import React, {useState} from 'react'
import {Link, useHistory, useParams} from 'react-router-dom'
import M from 'materialize-css'

const NewPassword = () => {
    const history = useHistory();
    const [password, setPassword] = useState("");
    const {token} = useParams();
    
    //console.log(token);

    const PostNewPasswordData = async () => {
        try {
            const response = await fetch("/newpassword", {
                method:"post",
                headers:{
                    "Content-Type":"application/json"
                },
                body:JSON.stringify({
                    password,
                    token
                })
            });

            const json = await response.json();

            if (json.error) {
                M.toast({html: json.error, classes:"#c62828 red darken-3"});
            }
            else {
                M.toast({html:json.message, classes:"#43a047 green darken-1"});
                history.push("/signin");
            }
        }
        catch(error) {
            console.log(error);
        }
    }

    return (
        <div className="mycard">
            <div className="card auth-card input-field">
                <h2>Instagram</h2>
                <input
                    type="password"
                    placeholder="Enter new password" 
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                />
                <button className="btn waves-effect waves-light #64b5f6 blue darken-1"
                onClick={() => PostNewPasswordData()}>
                    Reset Password
                </button>
                <h5>
                    <Link to="/signup">Don't have an account?</Link>
                </h5>                
            </div>
        </div>
    )
}

export default NewPassword;