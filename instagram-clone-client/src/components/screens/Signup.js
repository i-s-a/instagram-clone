import React, {useState, useEffect} from 'react'
import {Link, useHistory} from 'react-router-dom'
import M from 'materialize-css'

const Signup = () => {
    const history = useHistory();
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [imageFilename, setImageFilename] = useState("");
    const [imageUrl, setImageUrl] = useState(undefined);

    //When a user provides a profile picture, we only want to upload
    //the user details to our server when the imageUrl has been set
    useEffect(() => {
        async function uploadData() 
        {
            await uploadProfileFields();
        }

        if (imageUrl) {
            uploadData();            
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [imageUrl]);

    const uploadProfilePic = async () => {

        try {
            const data = new FormData();
            data.append("file", imageFilename);
            data.append("upload_preset", "instagram-clone");
            data.append("cloud_name", "mernimages");
            
            const response = await fetch("https://api.cloudinary.com/v1_1/mernimages/image/upload", {
                method:"post",
                body:data
            });

            const json = await response.json();
            setImageUrl(json.url);
        }
        catch (error) {
            console.log(error);
        }
    }

    const uploadProfileFields = async () => {
        //See https://emailregex.com/
        if (!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email)) {
            M.toast({html: "Invalid email", classes:"#c62828 red darken-3"});
            return;
        }

        try {
            const response = await fetch("/signup", {
                method:"post",
                headers:{
                    "Content-Type":"application/json"
                },
                body:JSON.stringify({
                    name,
                    email,
                    password,
                    photo:imageUrl ? imageUrl : undefined   //Only include the photo if we have an imageUrl, otherwise leave it undefined and the server will use a default photo
                })
            });

            const json = await response.json();

            if (json.error) {
                M.toast({html: json.error, classes:"#c62828 red darken-3"});
            }
            else {
                M.toast({html: json.message, classes:"#43a047 green darken-1"});
                history.push("/signin")
            }
        }
        catch (error) {
            console.log(error);
        }
    }

    const PostSignupData = async () => {

        try {
            if (imageFilename) {
                await uploadProfilePic();
            }
            else {
                await uploadProfileFields();
            }
        }
        catch(error) {
            console.log(error);
        }
    }

    return (
        <div className="mycard">
            <div className="card auth-card input-field">
                <h2>Instagram</h2>
                <input
                    type="text"
                    placeholder="name"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                />
                <input
                    type="text"
                    placeholder="email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                />
                <input
                    type="password"
                    placeholder="password" 
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                />
                <div className="file-field input-field">
                    <div className="btn #64b5f6 blue darken-1">
                        <span>Upload Profile Pic</span>
                        <input type="file" onChange={(e) => setImageFilename(e.target.files[0])} />
                    </div>
                    <div className="file-path-wrapper">
                        <input className="file-path validate" type="text" />
                    </div>
                </div>                
                <button className="btn waves-effect waves-light #64b5f6 blue darken-1"
                onClick={() => PostSignupData()}>
                    Sign Up
                </button>
                <h5>
                    <Link to="/signin">Already have an account?</Link>
                </h5>
            </div>
        </div>
    )
}

export default Signup;