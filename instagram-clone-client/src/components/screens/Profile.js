import React, {useEffect, useState, useContext} from 'react'
import {UserContext} from '../../App'

const Profile = () => {

    const [myPosts, setMyPosts] = useState([]);
    const {state, dispatch} = useContext(UserContext);
    const [imageFilename, setImageFilename] = useState("");
 
    useEffect(() => {
        async function fetchPosts() {
            try {
                const response = await fetch('/myposts', {
                    headers: {
                        "Authorization":"Bearer " + localStorage.getItem("jwt")
                    }
                })
                const posts = await response.json();
                setMyPosts(posts);
            }
            catch (error) {
                console.log(error);
            }
        }
        
        fetchPosts();

    }, []);

    useEffect(() => {
        async function updateProfile()
        {
            try {
                const data = new FormData();
                data.append("file", imageFilename);
                data.append("upload_preset", "instagram-clone");
                data.append("cloud_name", "mernimages");
                
                const cloudinaryResponse = await fetch("https://api.cloudinary.com/v1_1/mernimages/image/upload", {
                    method:"post",
                    body:data
                });

                const cloudinaryJson = await cloudinaryResponse.json();

                const response = await fetch('/updateprofilepic', {
                    method:"put",
                    headers: {
                        "Content-Type":"application/json",
                        "Authorization":"Bearer " + localStorage.getItem("jwt")                        
                    },
                    body:JSON.stringify({
                        photo:cloudinaryJson.url
                    })
                });

                const json = await response.json();

                localStorage.setItem("user", JSON.stringify({...state, photo:json.photo}));
                dispatch({type:"UPDATE_PHOTO", payload:json.photo});
            }
            catch(error) {
                console.log(error);
            }
        }

        if (imageFilename) {
            updateProfile();
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [imageFilename]);

    const updateProfilePic = (filename) => {
        setImageFilename(filename);
    }

    return (
        <div style={{maxWidth:"650px", margin:"0px auto"}}>
            <div style={{margin:"18px 0px",borderBottom:"1px solid grey"}}>
                <div style={{display:"flex", justifyContent:"space-around"}}>
                    <div>
                        <img style={{width:"160px", height:"160px", borderRadius:"80px", "margin-top":"20px"}}
                        src={state ? state.photo : "Loading..."}
                        alt="" />
                    </div>
                    <div>
                        <h4>{state ? state.name : "Loading..."}</h4>
                        <h5>{state ? state.email : "Loading..."}</h5>
                        <div style={{display:"flex", justifyContent:"space-between", width:"108%"}}>
                            <h6>{myPosts.length} posts</h6>
                            <h6>{state ? state.followers.length : "Loading..."} followers</h6>
                            <h6>{state ? state.following.length : "Loading..."} following</h6>
                        </div>
                    </div>
                </div>
                <div className="file-field input-field" style={{marging:"10px"}}>
                    <div className="btn #64b5f6 blue darken-1">
                        <span>Update Profile Pic</span>
                        <input type="file" onChange={(e) => updateProfilePic(e.target.files[0])} />
                    </div>
                    <div className="file-path-wrapper">
                        <input className="file-path validate" type="text" />
                    </div>
                </div>                </div>
            <div className="gallery">
                {
                    myPosts.map(item => {
                        return (
                            <img className="item" key={item._id} src={item.photo} alt={item.title} />
                        )
                    })
                }
            </div>
        </div>
    )
}

export default Profile;