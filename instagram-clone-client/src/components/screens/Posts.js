import React, {useState, useEffect, useContext} from 'react'
import {UserContext} from '../../App'
import {Link} from 'react-router-dom'

const Posts = ({fetchPostsUrl}) => {

    const [data, setData] = useState([]);
    const {state, dispatch} = useContext(UserContext);

    useEffect(() => {

        //If we visit the home page but the user is not logged in then
        //the router will navigate to the Signin page and this component
        //will unmount. If that happens and the request has already been sent
        //to the server then when we receive the response we will set the state
        //after we have unmounted. This will cause a memory leak as indicated by
        //React which generates a "Can't perform a React state update on an
        //unmounted component blah blah blah" message. To avoid this we will use
        //this flag to determine whether the component is still mounted and only
        //if it is will we update the state
        let isMounted = true;

        async function fetchPosts()
        {
            try {
                const response = await fetch(fetchPostsUrl, {
                    headers: {
                        "Authorization":"Bearer " + localStorage.getItem("jwt")
                    }
                });
                
                const posts = await response.json();

                if (isMounted) {
                    setData(posts);
                }
            }
            catch(error) {
                console.log(error);
            }
        }

        fetchPosts();

        //When the component unmounts set the isMounted flag to false so if the
        //fetch request returns after we've unmounted we will not set the state
        return () => {
            isMounted = false;
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const likePost = async (id) => {
        try {
            const response = await fetch('/like', {
                method:"put",
                headers: {
                    "Content-Type":"application/json",
                    "Authorization":"Bearer " + localStorage.getItem("jwt")
                },
                body:JSON.stringify({
                    postId:id
                })
            });

            const updatedPost = await response.json();

            const newData = data.map(item => {
                if (item._id === updatedPost._id) {
                    return updatedPost;
                }
                else {
                    return item;
                }
            })

            setData(newData);
        }
        catch (error) {
            console.log(error);
        }
    }

    const unlikePost = async (id) => {
        try {
            const response = await fetch('/unlike', {
                method:"put",
                headers: {
                    "Content-Type":"application/json",
                    "Authorization":"Bearer " + localStorage.getItem("jwt")
                },
                body:JSON.stringify({
                    postId:id
                })
            });

            const updatedPost = await response.json();

            const newData = data.map(item => {
                if (item._id === updatedPost._id) {
                    return updatedPost;
                }
                else {
                    return item;
                }
            });

            setData(newData);
        }
        catch (error) {
            console.log(error);
        }
    }

    const makeComment = async (text, postId) => {
        try {
            const response = await fetch('/comment', {
                method:"put",
                headers: {
                    "Content-Type":"application/json",
                    "Authorization":"Bearer " + localStorage.getItem("jwt")
                },
                body:JSON.stringify({
                    postId,
                    text
                })
            });

            const updatedPost = await response.json();

            const newData = data.map(item => {
                if (item._id === updatedPost._id) {
                    return updatedPost;
                }
                else {
                    return item;
                }
            })

            setData(newData);
        }
        catch (error) {
            console.log(error);
        }
    }

    const deletePost = async (postId) => {
        try {
            const response = await fetch(`/deletepost/${postId}`, {
                "method":"delete",
                headers: {
                    "Authorization":"Bearer " + localStorage.getItem("jwt")
                },
            });

            const deletedPost = await response.json();

            const newData = data.filter(item => {
                return item._id !== deletedPost._id;
            });

            setData(newData);    
        }
        catch (error) {
            console.log(error);
        }
    }

    const deleteComment = async (postId, commentId) => {
        try {

            const response = await fetch(`/deletecomment/${postId}/${commentId}`, {
                "method":"delete",
                headers: {
                    "Authorization":"Bearer " + localStorage.getItem("jwt")
                },
            });

            const updatedPost = await response.json();

            const newData = data.map(item => {
                if (item._id === updatedPost._id) {
                    //Use the response returned from the fetch request as this
                    //has the comment deleted from its list of comments
                    return updatedPost;
                }
                else {
                    //Use the one we already have in our data
                    return item;
                }
            })

            setData(newData);

        }
        catch (error) {
            console.log(error);
        }
    }    

    return (
        <div className="home">
            {
                data.map(item => {
                    return (
                        <div className="card home-card" key={item._id}>
                            <h5 style={{padding:"5px"}}>
                                <img 
                                    style={{width:"20px", height:"20px", borderRadius:"10px", marginRight:"5px"}}
                                    src={item.postedBy.photo}
                                    alt="" />

                                <Link to={item.postedBy._id !== state._id ? "/profile/" + item.postedBy._id : "/profile"}>{item.postedBy.name}</Link>
                                {item.postedBy._id === state._id &&
                                <i
                                    className="material-icons"
                                    style={{float:"right"}}
                                    onClick={() => deletePost(item._id)}>delete</i>}
                                {item.postedBy._id === state._id &&
                                <Link to={`/editpost/${item._id}`}>
                                    <i
                                    className="material-icons"
                                    style={{float:"right"}}>edit</i></Link>}
                            </h5>
                            <div className="card-image">
                                <img src={item.photo} alt={item.title} />
                            </div>
                            <div className="card-content">
                                <i className="material-icons" style={{color:"red"}}>favorite</i>
                                {item.likes.includes(state._id)
                                ? 
                                    <i className="material-icons" onClick={() => unlikePost(item._id)}>thumb_down</i>
                                :
                                    <i className="material-icons" onClick={() => likePost(item._id)}>thumb_up</i>
                                }
                                <h6>{item.likes.length} likes</h6>
                                <h6>{item.title}</h6>
                                <p>{item.body}</p>
                                {
                                    item.comments.map(c => {
                                        return (
                                            <h6 key={c._id}>
                                                <span style={{fontWeight:"500"}}>{c.postedBy.name}</span> {c.text}
                                                {c.postedBy._id === state._id && 
                                                <i
                                                    className="material-icons"
                                                    style={{float:"right"}}
                                                    onClick={() => deleteComment(item._id, c._id)}>delete</i>}
                                            </h6>
                                        )
                                    })
                                }
                                <form onSubmit={(e) => {
                                    e.preventDefault();
                                    makeComment(e.target[0].value, item._id);
                                }}>
                                    <input type="text" placeholder="Add a comment" />
                                </form>
                            </div>
                        </div>
                    );
                })
            }
        </div>
    )
}

export default Posts;