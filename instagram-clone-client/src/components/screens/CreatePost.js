import React, {useState, useEffect} from 'react'
import M from 'materialize-css'
import {useHistory} from 'react-router-dom'

const CreatePost = () => {

    const history = useHistory();
    const [title, setTitle] = useState("");
    const [body, setBody] = useState("");
    const [imageFilename, setImageFilename] = useState("");
    const [imageUrl, setImageUrl] = useState("");

    useEffect(() => {
        async function uploadPost()
        {
            await uploadPostDetails();
        }

        if (imageUrl) {
            uploadPost();
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [imageUrl]);

    const uploadPostDetails = async () => {
        try {
            const response = await fetch("/createpost", {
                method:"post",
                headers:{
                    "Content-Type":"application/json",
                    "Authorization":"Bearer " + localStorage.getItem("jwt")
                },
                body:JSON.stringify({
                    title,
                    body,
                    imageUrl
                })
            });

            const json = await response.json();
                
            if (json.error) {
                M.toast({html: json.error, classes:"#c62828 red darken-3"});
            }
            else {
                M.toast({html: "Post created successfully", classes:"#43a047 green darken-1"});
                history.push("/")
            }

        }
        catch (error) {
            console.log(error);
        }
    }

    //TODO: See this article
    //https://codeburst.io/react-image-upload-with-kittens-cc96430eaece
    //It includes a number of different scenarios that should be tested
    //when using Cloudinary. Make sure we can run the same tests and they
    //are handled gracefully by us here
    const uploadImage = async () => {

        try {
            const data = new FormData();
            data.append("file", imageFilename);
            data.append("upload_preset", "instagram-clone");
            data.append("cloud_name", "mernimages");
            
            const response = await fetch("https://api.cloudinary.com/v1_1/mernimages/image/upload", {
                method:"post",
                body:data
            });

            const json = await response.json();
            setImageUrl(json.url);
        }
        catch (error) {
            console.log(error);
        }
    }

    return (
        <div className="card input-field"
        style={{margin:"30px auto", maxWidth:"500px", padding:"20px", textAlign:"center"}}>
            <input
                type="text"
                placeholder="Title"
                value={title}
                onChange={(e) => setTitle(e.target.value)} />
            <input
                type="text"
                placeholder="Body"
                value={body}
                onChange={(e) => setBody(e.target.value)} />
            <div className="file-field input-field">
                <div className="btn #64b5f6 blue darken-1">
                    <span>Upload Image</span>
                    <input type="file" onChange={(e) => setImageFilename(e.target.files[0])} />
                </div>
                <div className="file-path-wrapper">
                    <input className="file-path validate" type="text" />
                </div>
            </div>
            <button className="btn waves-effect waves-light #64b5f6 blue darken-1"
            onClick={() => uploadImage()} >
                Submit Post
            </button>
        </div>
    )
}

export default CreatePost;