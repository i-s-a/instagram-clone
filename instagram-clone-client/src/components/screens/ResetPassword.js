import React, {useState} from 'react'
import {useHistory} from 'react-router-dom'
import M from 'materialize-css'

const ResetPassword = () => {
    const history = useHistory();
    const [email, setEmail] = useState("");

    const PostResetPasswordData = async () => {
        //See https://emailregex.com/
        if (!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email)) {
            M.toast({html: "Invalid email", classes:"#c62828 red darken-3"});
            return;
        }

        try {
            const response = await fetch("/resetpassword", {
                method:"post",
                headers:{
                    "Content-Type":"application/json"
                },
                body:JSON.stringify({
                    email,
                })
            });

            const json = await response.json();

            if (json.error) {
                M.toast({html: json.error, classes:"#c62828 red darken-3"});
            }
            else {
                M.toast({html:json.message, classes:"#43a047 green darken-1"});
                history.push("/signin");
            }
        }
        catch(error) {
            console.log(error);
        }
    }

    return (
        <div className="mycard">
            <div className="card auth-card input-field">
                <h2>Instagram</h2>
                <input
                    type="text"
                    placeholder="email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                />
                <button className="btn waves-effect waves-light #64b5f6 blue darken-1"
                onClick={() => PostResetPasswordData()}>
                    Reset Password
                </button>
            </div>
        </div>
    )
}

export default ResetPassword;