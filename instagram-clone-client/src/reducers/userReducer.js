export const initialState = null;

export const reducer = (state, action) => {

    //New user. Occurs when logging in and when the user edits their
    //details
    if (action.type === "USER") {
        return action.payload;
    }

    //Clear the user. Occurs when logging out
    if (action.type === "CLEAR") {
        return null;
    }
    
    //TODO: UPDATE only updates the followers and following. Perhaps rename
    //this to UPDATE_FF
    if (action.type === "UPDATE") {
        return {
            ...state,
            followers:action.payload.followers,
            following:action.payload.following
        }
    }

    //Profile photo has been updated
    if (action.type === "UPDATE_PHOTO") {
        return {
            ...state,
            photo:action.payload
        }
    }
    return state;
}