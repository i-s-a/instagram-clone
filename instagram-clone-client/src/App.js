import React, {useEffect, createContext, useReducer, useContext} from 'react';
import Navbar from './components/Navbar'
import './App.css'
import {BrowserRouter, Route, Switch, useHistory} from 'react-router-dom'
import Posts from './components/screens/Posts'
import Signin from './components/screens/Signin'
import Profile from './components/screens/Profile'
import Signup from './components/screens/Signup'
import CreatePost from './components/screens/CreatePost'
import UserProfile from './components/screens/UserProfile'
import ResetPassword from './components/screens/ResetPassword'
import NewPassword from './components/screens/NewPassword'
import EditPost from './components/screens/EditPost'
import {reducer, initialState} from './reducers/userReducer'

export const UserContext = createContext();

const Routing = () => {
  const history = useHistory();
  const {state, dispatch} = useContext(UserContext);

  useEffect(() => {
    const user = JSON.parse(localStorage.getItem("user"));
    if (user) {
      dispatch({type:"USER",payload:user});
      //history.push('/');
    }
    else {
      if (!history.location.pathname.startsWith('/resetpassword')) {
        history.push('/signin')
      }
    }

     // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Switch>
      <Route exact path="/">
        <Posts key="home" fetchPostsUrl="/allposts" />
      </Route>
      <Route path="/signin">
        <Signin key="signin" />
      </Route>
      <Route exact path="/profile">
        <Profile key="profile" />
      </Route>
      <Route path="/signup">
        <Signup key="signup" />
      </Route>
      <Route path="/create">
        <CreatePost key="createpost" />
      </Route>
      <Route path="/profile/:userId">
        <UserProfile key="userprofile" />
      </Route>
      <Route path="/followingposts">
        <Posts key="followingposts" fetchPostsUrl="/followingposts" />
      </Route>
      <Route exact path="/resetpassword">
        <ResetPassword key="resetpassword" />
      </Route>            
      <Route path="/resetpassword/:token">
        <NewPassword key="newpassword" />
      </Route>
      <Route path="/editpost/:postId">
        <EditPost key="editpost" />
      </Route>
    </Switch>
  )
}

function App() {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <UserContext.Provider value={{state, dispatch}}>
      <BrowserRouter>
        <Navbar />
        <Routing />
      </BrowserRouter>
    </UserContext.Provider>
  );
}

export default App;
