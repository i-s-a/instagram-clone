const express = require('express');
const app = express();
const mongoose = require('mongoose');
const PORT = process.env.PORT || 5000;

//TODO: See article
//https://medium.com/@thejasonfile/using-dotenv-package-to-create-environment-variables-33da4ac4ea8f
//Perhaps use dotenv to manage the keys (and maybe other currently hardcoded values like email from
//address, etc.) like described in the article
const {MONGOURI} = require('./config/keys');

// Make Mongoose use 'findOneAndUpdate()'.
// See https://stackoverflow.com/questions/52572852/deprecationwarning-collection-findandmodify-is-deprecated-use-findoneandupdate
mongoose.set('useFindAndModify', false);

mongoose.connect(MONGOURI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});

mongoose.connection.on('connected', () => {
    console.log("Connected to Mongo!");
});

mongoose.connection.on('error', (error) => {
    console.log("Error connecting to Mongo", error);
});

require('./models/user');
require('./models/post');

//Middleware to tell express to parse the body of incoming POST/PUT requests as JSON
app.use(express.json());

app.use(require('./routes/auth'));
app.use(require('./routes/post'));
app.use(require('./routes/user'));

if (process.env.NODE_ENV === "production") {
    app.use(express.static('instagram-clone-client/build'));
    const path = require('path');
    app.get("*", (req, res) => {
        res.sendFile(path.resolve(__dirname, 'instagram-clone-client', 'build', 'index.html'));
    })
}

app.listen(PORT, () => {
    console.log("Server is running on ", PORT);
})